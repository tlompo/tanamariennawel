# Cahier des charges
*Application bancaire ABC Bank* 
![header](https://raw.githubusercontent.com/MiyaOneTerrian/blabla/main/MicrosoftTeams-image%20(3).png)

**Nawel HAMED** 

**Tana LOMPO** 

**Marien MUPULU** 

**Décrivez brièvement l'objectif de l'application bancaire et les fonctionnalités qu'elle devrait offrir.** 

L'application bancaire a pour objectif de concevoir un système de compte bancaire simple qui permettra aux utilisateurs de réaliser des opérations courantes telles que le dépôt, le retrait d'argent et la consultation de leur historique de transactions.

Les fonctionnalités qu'elle devrait offrir sont les suivantes :  

- Consulter l’historique des transactions
- Réaliser les dépôts et le retrait d'argent
- Consulter et télécharger les relevés de compte
- Gestion du compte 
- Virement bancaire : permettre aux utilisateurs de transférer de l'argent à partir de leur compte bancaire vers un autre compte bancaire, en utilisant le numéro de compte et le code IBAN. (paylib, banques diverses)
- Bloquer la carte
- Authentification

**Énumérez les exigences techniques pour l'application, y compris la technologie à utiliser, la sécurité, la convivialité et la disponibilité.**

Voici les exigences techniques pour une application bancaire comprenant les fonctionnalités de dépôt, de retrait et de consultation de l'historique de compte :

**Technologie :**

Plateforme mobile : L'application doit être disponible sur iOS et Android.

Backend : Les technologies à utiliser sont : Java et Python.

Base de données : La base de données sera sur MongoDB car c’est une solution cloud qui s’adapte à plusieurs langages de programmation.

**Sécurité :**

Authentification : L'application doit permettre la double authentification de l'utilisateur via OTP ou de la biométrie ou un code secret.

Cryptage : Les données sensibles des clients doivent être anonymisées par un système de cryptage notamment pour les données personnelles, les numéros de compte bancaire et les détails de carte de crédit.

Sécurité réseau : Des tests d’intrusion système doivent être réalisés régulièrement afin de protéger l’application contre les attaques de type Denial of Service (DoS) ou Distributed Denial of Service (DDoS). L’application doit répondre aux exigences de test d’intrusion.

Sécurité des transactions : Les transactions financières doivent être protégées avec des protocoles de sécurité HTTPS, SSL et TLS pour protéger les données.

**Convivialité :**

Interface utilisateur conviviale : L'application doit être intuitive, ergonomique et responsive.

Personnalisation : Les utilisateurs doivent pouvoir personnaliser leur expérience utilisateurs en fonction de leurs préférences, telles que la langue ou encore le positionnement des onglets.

**Disponibilité :**

L'application doit être hautement disponible 24 heures sur 24, 7 jours sur 7 pour les clients afin qu'ils puissent effectuer des transactions à tout moment et ne doit pas subir de pannes fréquentes. 

Fiabilité : Les temps de réponse doivent être rapides pour garantir une expérience utilisateur optimale.

Tolérance aux pannes : L'application doit être capable de récupérer des pannes système rapidement et sans perturbation majeure pour les utilisateurs. Des mesures de maintenance régulières doivent être prises pour s'assurer que l'application est toujours disponible et fonctionne correctement.

Délai SLA : Temps d’intervention relativement court de l’ordre de quelques secondes.

**Décrivez les règles de gestion pour l'application, y compris les règles de gestion pour les comptes bancaires, les transactions et les utilisateurs.**

Les règles de gestion pour une application bancaire comprennent des règles pour les comptes bancaires, les transactions et les utilisateurs. 

Voici quelques règles de base pour chacun :

Règles de gestion pour les comptes bancaires :

- Un client peut être titulaire d'un seul compte dans une seule et même agence bancaire d’une même région. Il peut être en possession d’uniquement d’un seul compte d'épargne livret A.
- Un compte bancaire doit avoir un numéro unique et doit être associé à un client unique.
- Les types de comptes disponibles comprennent les comptes courants et les comptes d'épargne.
- Les comptes peuvent avoir des seuils de solde minimum et maximum, des taux d'intérêt et des frais associés.
- Les relevés de compte doivent être disponibles pour les clients à tout moment, et les relevés de compte papier peuvent être envoyés périodiquement par la poste si le client le souhaite.
- Consultation de l'historique de transactions : Permettre aux utilisateurs de consulter leur historique de transactions, y compris les dépôts, les retraits, les paiements effectués et les transferts de fonds.
- Service clientèle : Offrir un support clientèle pour aider les utilisateurs à résoudre les problèmes ou les questions liées à leur compte bancaire.

Règles de gestion pour les transactions :

- Les transactions doivent être associées à un compte unique et à un utilisateur unique.
- Les types de transactions disponibles comprennent les virements et les transferts.
- Les transactions doivent être autorisées par l'utilisateur approprié avant d'être effectuées par OTP.
- Les transactions peuvent être soumises à des limites de montant ou de fréquence.
- Un titulaire de compte peut effectuer une ou plusieurs transaction (s).
- Transfert d'argent : Permettre aux utilisateurs de transférer de l'argent entre différents comptes bancaires, qu'ils soient dans la même banque ou dans des banques différentes

Règles de gestion pour les utilisateurs :

- Les utilisateurs doivent être authentifiés avant de pouvoir accéder à leur compte ou effectuer une transaction.
- Les utilisateurs peuvent être classés selon leur rôle, tels que client ou employé de la banque.
- Les utilisateurs doivent avoir des identifiants uniques et des mots de passe sécurisés.
- Certains utilisateurs peuvent avoir des autorisations spéciales pour effectuer des tâches telles que l'ajout ou la suppression de comptes ou l'approbation de transactions.

**Modèle conceptuel des données**

![MCD](https://raw.githubusercontent.com/MiyaOneTerrian/blabla/main/MicrosoftTeams-image%20(2).png)

**Décrivez les fonctionnalités supplémentaires que vous aimeriez ajouter à l'application.**

Les fonctionnalités supplémentaires que nous aimerions ajouter sont : 

- Rejet de prélèvement
- Joindre les docs
- Prendre rdv
- Paiements mobiles : permettre aux utilisateurs de payer des factures ou des achats en magasin en utilisant leur téléphone mobile.
- Alertes de transaction : permettre aux utilisateurs de définir des alertes pour être notifiés lorsqu'il y a une activité inhabituelle sur leur compte bancaire, telle qu'une transaction inattendue.
- Paiements mobiles : intégrer des fonctionnalités de paiement mobile qui permettent aux utilisateurs de payer des factures, des achats en ligne ou dans des magasins physiques, ou de transférer de l'argent directement à d'autres utilisateurs via des applications tierces, comme PayPal ou Venmo.

**Décrivez les fonctionnalités que vous aimeriez ajouter à l'application si vous aviez plus de temps.**

- Contacter la banque et le SAV
- Augmenter le montant de retrait et plafond
- Afficher les comptes
- Conseils financiers : intégrer des outils et des ressources éducatives pour aider les utilisateurs à mieux comprendre les principes de base de la gestion financière, tels que la budgétisation, l'épargne, l'investissement, etc.
- Support client : fournir un accès facile aux services de support client pour aider les utilisateurs à résoudre tout problème ou question liée à leur compte bancaire ou à l'application elle-même.
- Paiements mobiles : Les utilisateurs pourraient effectuer des paiements à partir de leur téléphone portable, par exemple en scannant un code QR ou en utilisant une technologie NFC. Paiements mobiles : une fonctionnalité permettant aux utilisateurs de payer facilement avec leur téléphone en utilisant des services tels que Apple Pay ou Google
- Gestion des cartes de crédit : Les utilisateurs pourraient gérer leurs cartes

**Décrivez les fonctionnalités que vous aimeriez ajouter à l'application si vous aviez plus de budget.**

- Gestion de cartes : permettre aux utilisateurs de gérer les informations de leur carte de crédit/débit associée à leur compte bancaire, y compris la commande et l'activation de nouvelles cartes, la désactivation ou le blocage de cartes perdues ou volées, et la modification de la limite de dépenses. 
- Activation OTP
- Consulter son solde

**Élaborez un plan de livraison pour le projet, y compris les détails sur la façon dont le projet sera livré, le budget et le délai.**

![MCD](https://raw.githubusercontent.com/MiyaOneTerrian/blabla/main/MicrosoftTeams-image%20(1).png)

Plan de livraison pour le projet d'application bancaire :

- **Analyse des besoins et spécifications** : Cette phase prendra environ 20 jours et coûtera environ 30 000 €.
- **Conception de l'application** : Cette phase prendra environ 2 semaines et coûtera environ 20 000€.
- **Développement de l'application** : Cette phase prendra 2 mois et coûtera environ 500 000€.
- **Mise en pré-production et tests corrections :** Cette phase prendra 20 jours et coutera environ 5 000€
- **Déploiement et Mise en production :** Cette phase prendra environ 1 semaine et coûtera environ 10 000€
- **Formation et support :** Cette phase prendra environ trois semaines et coûtera environ 10 000€**.**
- **Délai de livraison estimé** : 5 mois 
- **Budget** : environ 575 000€.

**Précisez les détails sur la formation et le support technique à fournir aux employés de la banque.**

Pour former et soutenir les utilisateurs de l’application, des formations et du support seront mis en place :

Formation :

- Formation de compréhension de l'interface utilisateur de l'application bancaire, ses fonctionnalités principales et comment les utiliser pour traiter les demandes des clients.
- Formation des employés sur les mesures de sécurité mises en place pour protéger les informations sensibles des clients.

La formation devrait inclure une présentation détaillée des fonctionnalités clés de l'application bancaire, des instructions étape par étape sur la façon d'effectuer des opérations courantes telles que le dépôt, le retrait et la consultation de l'historique des transactions.

Support technique :

- Une équipe de support technique va être créer pour traiter tous les incidents de niveau 1, 2 et 3. Une base de connaissances contenant des réponses à des questions fréquentes, un guide utilisateur et des instructions détaillées sur la résolution de problèmes récurrents va être fourni aux utilisateurs.

**Décrivez les responsabilités de l'équipe de développement et la procédure de validation de l'application.**

L'équipe de développement à la responsabilité de concevoir, développer, tester et maintenir l'application en question, afin de s'assurer que l'application répond aux normes de sécurité et de confidentialité nécessaires pour le secteur bancaire. 

Ses principales responsabilités sont :

Compréhension du besoin du PO : L'équipe doit travailler en étroite collaboration avec les parties prenantes pour comprendre les besoins de l'application, les exigences de sécurité et de conformité, ainsi que les fonctionnalités nécessaires.

Conception : Sur la base des besoins identifiés, l'équipe doit concevoir l'architecture de l'application, les schémas de base de données, les modèles de sécurité, les workflows et les interfaces utilisateur.

Développement : L'équipe doit ensuite programmer l'application en utilisant les langages de programmation et les frameworks appropriés.

Test et assurance qualité : L'équipe doit tester l'application pour s'assurer qu'elle répond aux exigences fonctionnelles et de sécurité, effectuer des tests de performance, des tests d'intégration et des tests de pénétration, et s'assurer que l'application est conforme aux normes de sécurité et de confidentialité.

Mise en production et maintenance : L'équipe doit s'assurer que l'application est correctement déployée et configurée en production, surveiller les performances de l'application, corriger les bugs et les problèmes de sécurité, et mettre à jour l'application en fonction des besoins et des évolutions du secteur.

La procédure de validation de l’application bancaire est :

Audit de sécurité : L'application doit être soumise à un audit de sécurité pour identifier les vulnérabilités et les failles de sécurité.

Tests de pénétration : Des tests de pénétration doivent être effectués pour tenter d'exploiter les vulnérabilités identifiées lors de l'audit de sécurité.

Tests de conformité : L'application doit être testée pour s'assurer qu'elle est conforme aux réglementations de la RGPD et la conformité de la DSP2.

Tests de performance : L'application doit être testée pour s'assurer qu'elle peut supporter une charge élevée et fonctionner de manière fiable dans des conditions de stress.

**User story** 

1. En tant qu'utilisateur, je veux pouvoir déposer de l'argent dans mon compte bancaire en utilisant différentes méthodes telles que le virement bancaire, le dépôt direct ou le dépôt en espèces afin de pouvoir disposer de mon argent rapidement et facilement.
1. En tant qu'utilisateur, je veux pouvoir consulter mon historique de transactions pour savoir où j'ai dépensé mon argent, vérifier les détails des transactions individuelles et suivre les mouvements de fonds dans mon compte afin de mieux gérer mon argent.
1. En tant qu'utilisateur, je veux être en mesure de retirer de l'argent de mon compte bancaire en utilisant un guichet automatique ou en effectuant une demande de retrait à partir de l'application, afin de disposer de liquidités pour mes besoins.
1. En tant qu'utilisateur, je veux être en mesure de consulter l'historique de mes transactions afin de suivre mes dépenses et mes entrées d'argent.





